//
//  NSString+CMDQueryStringSerialization.m
//  CMDQueryStringSerialization
//
//  Created by Bryan Irace on 1/26/14.
//  Copyright (c) 2014 Caleb Davenport. All rights reserved.
//

#import "NSString+CMDQueryStringSerialization.h"

@implementation NSString (CMDQueryStringSerialization)

- (NSString *)CMDQueryStringSerialization_stringByAddingEscapes {
    NSString *string = [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet characterSetWithCharactersInString:@"!*'();:@&=+$,/?%#[]"]];
    return string;
}


- (NSString *)CMDQueryStringSerialization_stringByRemovingEscapes {
    return [self stringByRemovingPercentEncoding];
}

@end
